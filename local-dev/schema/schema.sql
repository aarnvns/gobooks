CREATE DATABASE IF NOT EXISTS library;
USE library;

CREATE TABLE books (
    id SERIAL,
    uuid VARCHAR(36) NOT NULL UNIQUE KEY,
    created_date DATETIME(3) NOT NULL,
    updated_date DATETIME(3) NOT NULL,
    title VARCHAR(100),
    description VARCHAR(100),
    author VARCHAR(100),
    category VARCHAR(100),
    isbn VARCHAR(30)
);
