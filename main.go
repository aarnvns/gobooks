package main

import (
	"log"
	"os"

	"asevans.com/library/handlers"
	"asevans.com/library/models"
)

func main() {
	databaseHost := os.Getenv("DATABASE_HOST")
	db, err := models.NewDB(models.DatabaseConfiguration{Username: "root", Password: "root", Host: databaseHost, Port: 3306, DatabaseName: "library"})
	if err != nil {
		log.Panic(err)
	}

	err = handlers.ListenAndServe(db)
	if err != nil {
		log.Panic(err)
	}
}
