package models

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	// Use the MySQL driver for database/sql
	_ "github.com/go-sql-driver/mysql"
)

// DB container type for sql.DB connections
type DB struct {
	*sql.DB
}

// EntityID is a database primary key
type EntityID int64

// ZeroEntityID for database IDs
const ZeroEntityID = EntityID(0)

// DatabaseConfiguration for NewDB
type DatabaseConfiguration struct {
	Username, Password, Host, DatabaseName string
	Port                                   uint32
}

// NewDB to create database template
func NewDB(config DatabaseConfiguration) (*DB, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", config.Username, config.Password, config.Host, config.Port, config.DatabaseName)
	log.Printf("Connecting to database: %s\n", dsn)

	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("sql dsn error %s: %v", dsn, err)
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("connection error to \"%s\": %v", dsn, err)
	}

	return &DB{db}, nil
}

// CurrentTimeForDB in a database compatible form
func CurrentTimeForDB() time.Time {
	loc, err := time.LoadLocation("UTC")
	if err != nil {
		log.Panic(fmt.Errorf("UTC location does not exist: %v", err))
	}
	return time.Now().In(loc).Truncate(time.Millisecond)
}
