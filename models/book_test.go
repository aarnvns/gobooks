package models

import (
	"os"
	"testing"

	"github.com/google/uuid"
)

func CreateDatabaseConfig() DatabaseConfiguration {
	databaseHost := os.Getenv("DATABASE_HOST")
	return DatabaseConfiguration{Username: "root", Password: "root", Host: databaseHost, Port: 3306, DatabaseName: "library"}
}

func TestInsertBook(t *testing.T) {
	book := Book{Title: "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	testStartTime := CurrentTimeForDB()

	if err := db.InsertBook(&book); err != nil {
		t.Errorf("error inserting book: %v", err)
	}
	testEndTime := CurrentTimeForDB()

	if book.ID == 0 {
		t.Error("books must have IDs")
	}

	if book.UUID == *new(uuid.UUID) {
		t.Error("no UUID assigned to book")
	}

	if testStartTime.After(book.CreatedDate) || testEndTime.Before(book.CreatedDate) {
		t.Errorf("invalid CreatedDate for book: %v", book.CreatedDate)
	}

	if testStartTime.After(book.UpdatedDate) || testEndTime.Before(book.UpdatedDate) {
		t.Errorf("invalid UpdateDate for book: %v", book.UpdatedDate)
	}

	lookup, err := db.FindBook(book.ID)
	if err != nil {
		t.Errorf("error loading book %d: %v", book.ID, err)
	}
	if *lookup != book {
		t.Errorf("Inserted book does not match\nfound: %v\nexpected: %v\n", *lookup, book)
	}
}

func TestInsertBookHasID(t *testing.T) {
	book := Book{ID: EntityID(1),
		Title:       "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	// TODO: check created/updated timestamps
	if db.InsertBook(&book) == nil {
		t.Error("expected error inserting a book with duplicate ID")
	}
}

func TestInsertBookWithUUID(t *testing.T) {
	originalUUID := uuid.New()
	book := Book{UUID: originalUUID,
		Title:       "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	if err := db.InsertBook(&book); err != nil {
		t.Fatalf("Failed inserting book: %v", err)
	}

	lookup, err := db.FindBook(book.ID)
	if err != nil {
		t.Errorf("error loading book %d: %v", book.ID, err)
	}
	if lookup.UUID != originalUUID {
		t.Errorf("Inserted book was given a new UUID; found: %v expected: %v\n", lookup.UUID, originalUUID)
	}
	if *lookup != book {
		t.Errorf("Inserted book does not match\nfound: %v\nexpected: %v\n", *lookup, book)
	}
}

func TestUpdateBook(t *testing.T) {
	book := Book{Title: "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	if err := db.InsertBook(&book); err != nil {
		t.Fatalf("error inserting book: %v", err)
	}

	book.Title = "test title 2"
	book.Description = "test description 2"
	book.Author = "test author 2"
	book.Category = "test category 2"
	book.Isbn = "test isbn 2"

	if db.UpdateBook(&book) != nil {
		t.Errorf("error updating book: %v", err)
	}

	lookup, err := db.FindBook(book.ID)
	if err != nil {
		t.Errorf("error loading book %d: %v", book.ID, err)
	}

	if *lookup != book {
		t.Errorf("Inserted book does not match\nfound: %v\nexpected: %v\n", *lookup, book)
	}
}

func TestUpdateBookZeroID(t *testing.T) {
	book := Book{Title: "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	if err := db.UpdateBook(&book); err == nil {
		t.Error("book with null ID should fail to update")
	}
}

func TestFindBook(t *testing.T) {
	book1 := Book{Title: "test title 1",
		Description: "test description 1",
		Author:      "test author 1",
		Category:    "test category 1",
		Isbn:        "test isbn 1"}

	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	if err := db.InsertBook(&book1); err != nil {
		t.Fatalf("failed to insert book: %v", err)
	}

	lookupBook, err := db.FindBook(book1.ID)
	if err != nil {
		t.Errorf("error loading book %d: %v", book1.ID, err)
	}

	if book1 != *lookupBook {
		t.Errorf("Not equal\nfound: %v\nexpected: %v", *lookupBook, book1)
	}
}

func TestFindBookNotExisting(t *testing.T) {
	db, err := NewDB(CreateDatabaseConfig())
	if err != nil {
		t.Fatalf("connection error: %v\n", err)
	}

	lookupBook, err := db.FindBook(ZeroEntityID)
	if err != nil {
		t.Errorf("error loading book %d: %v", ZeroEntityID, err)
	}
	if lookupBook != nil {
		t.Errorf("FindBook returned record; nil expected: %v", lookupBook)
	}
}
