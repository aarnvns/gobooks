package models

import (
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
)

// Book details stored by database
type Book struct {
	ID          EntityID  `json:"id"`
	UUID        uuid.UUID `json:"uuid"`
	CreatedDate time.Time `json:"created_date"`
	UpdatedDate time.Time `json:"updated_date"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Author      string    `json:"author"`
	Category    string    `json:"category"`
	Isbn        string    `json:"isbn"`
}

// BookDatastore is a DAO for Books
type BookDatastore interface {
	InsertBook(book *Book) error
	UpdateBook(book *Book) error
	AllBooks() ([]*Book, error)
	FindBook(EntityID) (*Book, error)
}

// InsertBook new book into the database
func (db *DB) InsertBook(book *Book) error {
	if book.ID != ZeroEntityID {
		return fmt.Errorf("cannot insert book having an ID; found: %v", book.ID)
	}

	if book.UUID == *new(uuid.UUID) {
		book.UUID = uuid.New()
	}

	book.CreatedDate = CurrentTimeForDB()
	book.UpdatedDate = book.CreatedDate

	res, err := db.Exec("INSERT INTO books (uuid, created_date, updated_date, title, description, author, category, isbn) values (?, ?, ?, ?, ?, ?, ?, ?)",
		book.UUID, book.CreatedDate, book.UpdatedDate, book.Title, book.Description, book.Author, book.Category, book.Isbn)
	if err != nil {
		return fmt.Errorf("error executing insert statement: %v", err)
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		return fmt.Errorf("error getting inserted ID: %v", err)
	}

	book.ID = EntityID(lastID)
	return nil
}

// UpdateBook modifies records already in the database
func (db *DB) UpdateBook(book *Book) error {
	book.UpdatedDate = CurrentTimeForDB()

	res, err := db.Exec("UPDATE books SET updated_date = ?, title = ?, description = ?, author = ?, category = ?, isbn = ? WHERE id = ?",
		book.UpdatedDate, book.Title, book.Description, book.Author, book.Category, book.Isbn, book.ID)
	if err != nil {
		return fmt.Errorf("error updating statement: %v", err)
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("error counting rows affected by update: %v", err)
	}

	if rows != 1 {
		return fmt.Errorf("update affected %d rows; expected 1", rows)
	}

	return nil
}

// AllBooks returns all records of Book entity
func (db *DB) AllBooks() ([]*Book, error) {
	rows, err := db.Query("SELECT * FROM books")
	if err != nil {
		return nil, fmt.Errorf("error querying all books: %v", err)
	}
	defer rows.Close()

	books := make([]*Book, 0)
	for rows.Next() {
		book := new(Book)
		err := rows.Scan(&book.ID, &book.UUID, &book.CreatedDate, &book.UpdatedDate, &book.Title, &book.Description, &book.Author, &book.Category, &book.Isbn)
		if err != nil {
			log.Fatalf("%v", err)
			return nil, fmt.Errorf("error loading book from list: %v", err)
		}
		books = append(books, book)
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("rows error: %v", err)
	}
	return books, nil
}

// FindBook returns an (optional) book
// nil if no result found.
func (db *DB) FindBook(entityID EntityID) (*Book, error) {
	rows, err := db.Query("SELECT * FROM books WHERE id = ?", entityID)
	if err != nil {
		return nil, fmt.Errorf("error on FindBook query: %v", err)
	}
	defer rows.Close()

	var book *Book
	if rows.Next() {
		book = new(Book)
		err := rows.Scan(&book.ID, &book.UUID, &book.CreatedDate, &book.UpdatedDate, &book.Title, &book.Description, &book.Author, &book.Category, &book.Isbn)
		if err != nil {
			return nil, fmt.Errorf("error loading book from list: %v", err)
		}
	}
	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("rows error: %v", err)
	}
	return book, nil
}
