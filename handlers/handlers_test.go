package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"asevans.com/library/models"
)

func TestListBooks(t *testing.T) {
	book1 := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}
	book2 := models.Book{Title: "title2", Description: "description2", Category: "category2", Isbn: "isbn2"}

	env := newEnv()
	env.DB.InsertBook(&book1)
	env.DB.InsertBook(&book2)

	rec := env.mockRequest(t, "GET", "/books", "")

	checkResponseCode(t, 200, rec)
	checkJSONOutput(t, [2]models.Book{book1, book2}, rec)
	checkResponseType(t, "application/json", rec)
}

func TestFindBook(t *testing.T) {
	book1 := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}
	book2 := models.Book{Title: "title2", Description: "description2", Category: "category2", Isbn: "isbn2"}

	env := newEnv()
	env.DB.InsertBook(&book1)
	env.DB.InsertBook(&book2)

	rec := env.mockRequest(t, "GET", fmt.Sprintf("/books/%d", book1.ID), "")

	checkResponseCode(t, 200, rec)
	checkJSONOutput(t, book1, rec)
	checkResponseType(t, "application/json", rec)
}

func TestFindBookNotFound(t *testing.T) {
	book := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}

	env := newEnv()
	env.DB.InsertBook(&book)

	rec := env.mockRequest(t, "GET", fmt.Sprintf("/books/%d", models.EntityID(*nextBookID+1)), "")

	checkResponseCode(t, http.StatusNotFound, rec)
	checkStringOutput(t, "404 page not found\n", rec)
	checkResponseType(t, "text/plain; charset=utf-8", rec)
}

func TestFindBookBadID(t *testing.T) {
	env := newEnv()

	rec := env.mockRequest(t, "GET", "/books/x", "")

	checkResponseCode(t, http.StatusBadRequest, rec)
	checkStringOutput(t, "invalid id: x\n", rec)
	checkResponseType(t, "text/plain; charset=utf-8", rec)
}

func TestUpdateBook(t *testing.T) {
	book := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}

	env := newEnv()
	env.DB.InsertBook(&book)

	updatedBook := book
	updatedBook.Title = "title2"
	updatedBook.Description = "description2"
	updatedBook.Category = "category2"
	updatedBook.Isbn = "isbn2"

	marshalled, err := json.Marshal(updatedBook)
	if err != nil {
		t.Fatalf("error marshalling book: %v", err)
	}

	rec := env.mockRequest(t, "PUT", fmt.Sprintf("/books/%d", updatedBook.ID), string(marshalled))

	checkResponseCode(t, 200, rec)
	checkJSONOutput(t, updatedBook, rec)
	checkResponseType(t, "application/json", rec)
}

func TestUpdateBookNotFound(t *testing.T) {
	book := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}

	env := newEnv()
	env.DB.InsertBook(&book)

	updatedBook := book
	updatedBook.ID = models.EntityID(*nextBookID + 1)
	updatedBook.Title = "title2"
	updatedBook.Description = "description2"
	updatedBook.Category = "category2"
	updatedBook.Isbn = "isbn2"

	marshalled, err := json.Marshal(updatedBook)
	if err != nil {
		t.Fatalf("error marshalling book: %v", err)
	}

	rec := env.mockRequest(t, "PUT", fmt.Sprintf("/books/%d", updatedBook.ID), string(marshalled))

	checkResponseCode(t, http.StatusNotFound, rec)
	checkResponseType(t, "text/plain; charset=utf-8", rec)
}

func TestUpdateBookBadContent(t *testing.T) {
	book := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}

	env := newEnv()
	env.DB.InsertBook(&book)

	updatedBook := book
	updatedBook.Title = "title2"
	updatedBook.Description = "description2"
	updatedBook.Category = "category2"
	updatedBook.Isbn = "isbn2"

	rec := env.mockRequest(t, "PUT", fmt.Sprintf("/books/%d", updatedBook.ID), "this is not valid JSON")

	checkResponseCode(t, http.StatusBadRequest, rec)
}

func TestCreateBook(t *testing.T) {
	book := models.Book{Title: "title1", Description: "description1", Category: "category1", Isbn: "isbn1"}

	env := newEnv()

	marshalled, err := json.Marshal(book)
	if err != nil {
		t.Fatalf("error marshalling book: %v", err)
	}

	rec := env.mockRequest(t, "POST", "/books", string(marshalled))

	checkResponseCode(t, 200, rec)
	checkResponseType(t, "application/json", rec)

	var createdBook models.Book
	if err := json.Unmarshal([]byte(rec.Body.String()), &createdBook); err != nil {
		t.Errorf("error unmarshalling response: %v", err)
	}

	book.ID = createdBook.ID
	book.UUID = createdBook.UUID
	book.CreatedDate = createdBook.CreatedDate
	book.UpdatedDate = createdBook.UpdatedDate
	if book != createdBook {
		t.Errorf("expected = %v\n...obtained = %v", book, createdBook)
	}
}

func TestCreateBookBadContent(t *testing.T) {
	env := newEnv()
	rec := env.mockRequest(t, "POST", "/books", "this is invalid JSON")

	checkResponseCode(t, http.StatusBadRequest, rec)
}

func newEnv() Env {
	return Env{DB: &mockDB{books: make(map[models.EntityID]models.Book)}}
}

func (env *Env) mockRequest(t *testing.T, method, url string, body string) *httptest.ResponseRecorder {
	req, err := http.NewRequest(method, url, strings.NewReader(body))
	if err != nil {
		t.Fatalf("error creating request: %v", err)
	}

	rec := httptest.NewRecorder()
	r := env.NewRouter()
	r.ServeHTTP(rec, req)
	return rec
}

func checkResponseCode(t *testing.T, expected int, rec *httptest.ResponseRecorder) {
	if expected != rec.Result().StatusCode {
		t.Errorf("Expected response code %d. Got %d\n", expected, rec.Result().StatusCode)
	}
}

func checkStringOutput(t *testing.T, exp string, rec *httptest.ResponseRecorder) {
	if exp != rec.Body.String() {
		t.Errorf("expected = %v\n...obtained = %v", exp, rec.Body.String())
	}
}

func checkJSONOutput(t *testing.T, exp interface{}, rec *httptest.ResponseRecorder) {
	expected, err := json.Marshal(exp)
	if err != nil {
		t.Fatalf("marshalling error: %v", err)
	}
	if string(expected) != rec.Body.String() {
		t.Errorf("\n...expected = %v\n...obtained = %v", string(expected), rec.Body.String())
	}
}

func checkResponseType(t *testing.T, exp string, rec *httptest.ResponseRecorder) {
	contentType := rec.Header()["Content-Type"]
	if len(contentType) != 1 || contentType[0] != exp {
		t.Errorf("content-type did not match expected (%s): %v", exp, contentType)
	}
}
