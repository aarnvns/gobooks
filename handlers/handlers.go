package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"asevans.com/library/models"
	"github.com/gorilla/mux"
)

// Env dependency-injection container for application
type Env struct {
	DB models.BookDatastore
}

// NewRouter creates request router
func (env Env) NewRouter() *mux.Router {
	r := mux.NewRouter()
	r.Methods("GET").Path("/books/{id}").HandlerFunc(env.findBook)
	r.Methods("GET").Path("/books").HandlerFunc(env.listBooks)
	r.Methods("PUT").Path("/books/{id}").HandlerFunc(env.updateBook)
	r.Methods("POST").Path("/books").HandlerFunc(env.createBook)
	return r
}

// ListenAndServe wires up handlers and starts listening on a port for HTTP
func ListenAndServe(db *models.DB) error {
	http.Handle("/", Env{DB: db}.NewRouter())
	err := http.ListenAndServe(":3000", nil)
	return err
}

// listBooks serves GET books/
func (env *Env) listBooks(w http.ResponseWriter, r *http.Request) {
	books, err := env.DB.AllBooks()
	if err != nil {
		http.Error(w, fmt.Sprintf("error reading database: %v", err), http.StatusInternalServerError)
		return
	}
	formatted, err := json.Marshal(books)
	if err != nil {
		http.Error(w, fmt.Sprintf("error marshalling response: %v", err), http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%v", string(formatted))
}

// findBook looks up a single findBook
func (env *Env) findBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	strID, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(w, fmt.Sprintf("invalid id: %s", vars["id"]), http.StatusBadRequest)
		return
	}

	id := models.EntityID(int64(strID))
	book, err := env.DB.FindBook(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if book == nil {
		http.NotFound(w, r)
		return
	}

	formatted, err := json.Marshal(book)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%v", string(formatted))
}

func (env *Env) updateBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	strID, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(w, fmt.Sprintf("invalid ID: %v", vars["id"]), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("error reading input: %v", err), 500)
		return
	}

	book := models.Book{}
	err = json.Unmarshal(body, &book)
	if err != nil {
		http.Error(w, fmt.Sprintf("error unmarshalling input: %v", err), http.StatusBadRequest)
		return
	}

	loadBook, err := env.DB.FindBook(models.EntityID(int64(strID)))
	if err != nil {
		http.Error(w, fmt.Sprintf("error reading book: %v", err), 500)
		return
	}

	if loadBook == nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}

	loadBook.UpdatedDate = models.CurrentTimeForDB()
	loadBook.Title = book.Title
	loadBook.Description = book.Description
	loadBook.Author = book.Author
	loadBook.Category = book.Category
	loadBook.Isbn = book.Isbn

	env.DB.UpdateBook(loadBook)

	formatted, err := json.Marshal(book)
	if err != nil {
		http.Error(w, fmt.Sprintf("error marshalling book: %v", err), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%v", string(formatted))
}

func (env *Env) createBook(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("error reading input: %v", err), 500)
		return
	}

	book := models.Book{}
	err = json.Unmarshal(body, &book)
	if err != nil {
		http.Error(w, fmt.Sprintf("error unmarshalling input: %v", err), http.StatusBadRequest)
		return
	}

	env.DB.InsertBook(&book)
	if err != nil {
		http.Error(w, fmt.Sprintf("error reading book: %v", err), 500)
		return
	}

	formatted, err := json.Marshal(book)
	if err != nil {
		http.Error(w, fmt.Sprintf("error marshalling book: %v", err), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%v", string(formatted))
}
