package handlers

import (
	"sync/atomic"

	"asevans.com/library/models"
	"github.com/google/uuid"
)

type mockDB struct {
	books map[models.EntityID]models.Book
}

var nextBookID *int64 = new(int64)

func (mdb *mockDB) InsertBook(book *models.Book) error {
	book.ID = models.EntityID(*nextBookID)
	book.UUID = uuid.New()
	book.CreatedDate = models.CurrentTimeForDB()
	book.UpdatedDate = models.CurrentTimeForDB()

	atomic.AddInt64(nextBookID, 1)
	mdb.books[book.ID] = *book
	return nil
}

func (mdb *mockDB) AllBooks() ([]*models.Book, error) {
	books := make([]*models.Book, 0)
	for _, book := range mdb.books {
		book := book
		books = append(books, &book)
	}
	return books, nil
}

func (mdb *mockDB) FindBook(id models.EntityID) (*models.Book, error) {
	book, ok := mdb.books[id]
	if !ok {
		return nil, nil
	}
	return &book, nil
}

func (mdb *mockDB) UpdateBook(book *models.Book) error {
	mdb.books[book.ID] = *book
	return nil
}
